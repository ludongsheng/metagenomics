# 安装igraph和pysch包，已经安装过可跳过
# 用library(igraph)和library(psych)测试是否安装了这两个包
install.packages("igraph")  # 不能正常安装可能是c compile的问题
install.packages("psych")

setwd('Desktop/test/') # 工作目录，假设test/文件夹下有otu_table.txt和otu_pro.txt
library(igraph)
library(psych)

otu = read.table("otu_table.txt", head=T, row.names=1)
occor = corr.test(otu, use="pairwise", method="spearman", adjust="fdr", alpha=.05)
occor.r = occor$r
occor.p = occor$p
occor.r[occor.p>0.05|abs(occor.r)<0.6] = 0

igraph = graph_from_adjacency_matrix(occor.r, mode="undirected", weighted=TRUE, diag=FALSE)
bad.vs = V(igraph)[degree(igraph) == 0] # 此处会有过滤过程
igraph = delete.vertices(igraph, bad.vs)
igraph.weight = E(igraph)$weight  # 赋值图的权重
E(igraph)$weight = NA

# 图例1
set.seed(123)
plot(igraph, main="Co-occurrence network", vertex.frame.color=NA, vertex.label=NA, edge.width=1, vertex.size=5, edge.lty=1, edge.curved=TRUE, margin=c(0,0,0,0))

# 设置边颜色
sum(igraph.weight > 0)# number of postive correlation
sum(igraph.weight < 0)# number of negative correlation
E.color = igraph.weight
E.color = ifelse(E.color>0, "red", ifelse(E.color < 0, "blue","grey"))
E(igraph)$color = as.character(E.color)
set.seed(123)
plot(igraph, main="Co-occurrence network", vertex.frame.color=NA, vertex.label=NA, edge.width=1, vertex.size=5, edge.lty=1, edge.curved=TRUE, margin=c(0,0,0,0))

# 设置边宽度
E(igraph)$width = abs(igraph.weight)*4
set.seed(123)
plot(igraph, main="Co-occurrence network", vertex.frame.color=NA, vertex.label=NA, vertex.size=5, edge.lty=1, edge.curved=TRUE, margin=c(0,0,0,0))

# 设置点的颜色和大小属性
otu_pro = read.table("otu_pro.txt", head=T, row.names=1)
igraph.size = otu_pro[V(igraph)$name,] # 筛选对应OTU属性
igraph.size1 = log((igraph.size$abundance)*100) # 原始数据是什么，为什么*100
V(igraph)$size = igraph.size1
igraph.col = otu_pro[V(igraph)$name,]
levels(igraph.col$phylum)
levels(igraph.col$phylum) = c("green","deeppink","deepskyblue","yellow","brown","pink","gray","cyan","peachpuff") # 直接修改levles可以连值全部对应替换
V(igraph)$color = as.character(igraph.col$phylum)
set.seed(123)
plot(igraph,main="Co-occurrence network", vertex.frame.color=NA, vertex.label=NA, edge.lty=1, edge.curved=TRUE, margin=c(0,0,0,0))

# 调整布局
set.seed(123)
plot(igraph, main="Co-occurrence network", layout=layout_with_kk, vertex.frame.color=NA, vertex.label=NA, edge.lty=1, edge.curved=TRUE, margin=c(0,0,0,0))
set.seed(123)
plot(igraph, main="Co-occurrence network", layout=layout.fruchterman.reingold, vertex.frame.color=NA, vertex.label=NA, edge.lty=1, edge.curved=TRUE, margin=c(0,0,0,0))

# 按模块着色
fc = cluster_fast_greedy(igraph, weights =NULL) # cluster_walktrap cluster_edge_betweenness, cluster_fast_greedy, cluster_spinglass
modularity = modularity(igraph,membership(fc))
comps = membership(fc)
colbar = rainbow(max(comps))
V(igraph)$color = colbar[comps]
set.seed(123)
plot(igraph, main="Co-occurrence network", vertex.frame.color=NA, vertex.label=NA, edge.lty=1, edge.curved=TRUE, margin=c(0,0,0,0))

# 显示标签和点轮廓
plot(igraph, main="Co-occurrence network", vertex.frame.color=NA, vertex.label=V(igraph)$name, edge.lty=1, edge.curved=TRUE, margin=c(0,0,0,0))